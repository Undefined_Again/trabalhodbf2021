package com.davysalgado.atividade.repository;

import com.davysalgado.atividade.domain.Exercicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExercicioRepositorio extends JpaRepository<Exercicio, Long> {
}
