package com.davysalgado.atividade.web;

import com.davysalgado.atividade.domain.Usuario;
import com.davysalgado.atividade.service.UsuarioService;
import org.apache.catalina.Store;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

    private final Logger log = LoggerFactory.getLogger(UsuarioResource.class);
    private final UsuarioService usuarioService;

    public UsuarioResource(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }
//    Aula 1, não usar. Comunicação diferente para inserir. Usar post para inserir e não get.
//    @GetMapping(path = "/criar/{name}")
//    public String criar(@PathVariable String name){
//        Usuario u = new Usuario();
//        u.setNome(name);
//        usuarioService.save(u);
//        return "Criado com sucesso";
//    }
//
//    @GetMapping(path = "/listar/{id}")
//    public Usuario listar(@PathVariable Long id){
//        return usuarioService.findOne(id).get();
//    }

    @GetMapping("/{id}")
    public ResponseEntity<Usuario> getUsuario(@PathVariable Long id){
        log.debug("REST request to getUsuario: {}", id);
        Optional<Usuario> usuario=usuarioService.findOne(id);
        if(usuario.isPresent()){
            return ResponseEntity.ok().body(usuario.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<Usuario>> getUsuarios(){
        List<Usuario> lista= usuarioService.findAllList();
        if(lista.size()>0){
            return ResponseEntity.ok().body(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/")
    public ResponseEntity<Usuario> updateUsuario(@RequestBody Usuario usuario) throws URISyntaxException {
        log.debug("REST request to updateUsuario: {}", usuario);
        if(usuario.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuário invalido, id nulo");
        }
        Usuario result =usuarioService.save(usuario);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/")
    public ResponseEntity<Usuario> createUsuario(@RequestBody Usuario usuario) throws URISyntaxException {
        log.debug("REST request to createUsuario: {}", usuario);
        if (usuario.getId() != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um novo usuário não pode ter um ID, ID é gerado automaticamente pelo banco de dados");
        }
        Usuario result= usuarioService.save(usuario);
        return ResponseEntity.created(new URI("/api/usuarios/" + result.getId())).body(result);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Void> deleteUsuario(@PathVariable Long id) {
        this.log.debug("REST request to deleteUsuario : {}", id);
        this.usuarioService.delete(id);
        return ResponseEntity.noContent().build();
    }

}

