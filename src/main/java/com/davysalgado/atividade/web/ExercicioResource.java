package com.davysalgado.atividade.web;

import com.davysalgado.atividade.domain.Exercicio;
import com.davysalgado.atividade.service.ExercicioService;
import org.apache.catalina.Store;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/exercicios")
public class ExercicioResource {

    private final Logger log = LoggerFactory.getLogger(ExercicioResource.class);
    private final ExercicioService exercicioService;

    public ExercicioResource(ExercicioService exercicioService) {
        this.exercicioService = exercicioService;
    }
    //Aula 1, não usar. Comunicação diferente para inserir. Usar post para inserir e não get.
//    @GetMapping(path = "/criar/{enunciado}")
//    public String criar(@PathVariable String enunciado){
//        Exercicio e = new Exercicio();
//        e.setEnunciado(enunciado);
//        exercicioService.save(e);
//        return "Criado com sucesso";
//    }
//
//    @GetMapping(path = "/listar/{id}")
//    public Exercicio listar(@PathVariable Long id){
//        return exercicioService.findOne(id).get();
//    }

    @GetMapping("/{id}")
    public ResponseEntity<Exercicio> getExercicio(@PathVariable Long id){
        log.debug("REST request to getExercicio: {}", id);
        Optional<Exercicio> exercicio=exercicioService.findOne(id);
        if(exercicio.isPresent()){
            return ResponseEntity.ok().body(exercicio.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<Exercicio>> getExercicios(){
        List<Exercicio> lista= exercicioService.findAllList();
        if(lista.size()>0){
            return ResponseEntity.ok().body(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/")
    public ResponseEntity<Exercicio> updateExercicios(@RequestBody Exercicio exercicio) throws URISyntaxException {
        log.debug("REST request to updateExercicios: {}", exercicio);
        if(exercicio.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuário invalido, id nulo");
        }
        Exercicio result =exercicioService.save(exercicio);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/")
    public ResponseEntity<Exercicio> createExercicio(@RequestBody Exercicio exercicio) throws URISyntaxException {
        log.debug("REST request to createExercicio: {}", exercicio);
        if (exercicio.getId() != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um novo usuário não pode ter um ID, ID é gerado automaticamente pelo banco de dados");
        }
        Exercicio result= exercicioService.save(exercicio);
        return ResponseEntity.created(new URI("/api/exercicios/" + result.getId())).body(result);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Void> deleteExercicios(@PathVariable Long id) {
        this.log.debug("REST request to deleteExercicios : {}", id);
        this.exercicioService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
