package com.davysalgado.atividade.service;

import com.davysalgado.atividade.domain.Exercicio;
import com.davysalgado.atividade.repository.ExercicioRepositorio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExercicioService {

    private final Logger log = LoggerFactory.getLogger(ExercicioService.class);

    private final ExercicioRepositorio exercicioRepositorio;

    public ExercicioService(ExercicioRepositorio exercicioRepositorio) {
        this.exercicioRepositorio = exercicioRepositorio;
    }

    public List<Exercicio> findAllList(){
        log.debug("Request to get All Exercicio");
        return exercicioRepositorio.findAll();
    }

    public Optional<Exercicio> findOne(Long id) {
        log.debug("Request to get Exercicio : {}", id);
        return exercicioRepositorio.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Exercicio : {}", id);
        exercicioRepositorio.deleteById(id);
    }

    public Exercicio save(Exercicio exercicio) {
        log.debug("Request to save Exercicio : {}", exercicio);
        exercicio = exercicioRepositorio.save(exercicio);
        return exercicio;
    }
}

