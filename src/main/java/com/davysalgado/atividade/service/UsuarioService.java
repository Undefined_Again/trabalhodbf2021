package com.davysalgado.atividade.service;

import com.davysalgado.atividade.domain.Usuario;
import com.davysalgado.atividade.repository.UsuarioRepositorio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    private final Logger log = LoggerFactory.getLogger(UsuarioService.class);

    private final UsuarioRepositorio usuarioRepositorio;

    public UsuarioService(UsuarioRepositorio usuarioRepositorio) {
        this.usuarioRepositorio = usuarioRepositorio;
    }

    public List<Usuario> findAllList(){
        log.debug("Request to get All Usuario");
        return usuarioRepositorio.findAll();
    }

    public Optional<Usuario> findOne(Long id) {
        log.debug("Request to get Usuario : {}", id);
        return usuarioRepositorio.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Usuario : {}", id);
        usuarioRepositorio.deleteById(id);
    }

    public Usuario save(Usuario usuario) {
        log.debug("Request to save Usuario : {}", usuario);
        usuario = usuarioRepositorio.save(usuario);
        return usuario;
    }

}
