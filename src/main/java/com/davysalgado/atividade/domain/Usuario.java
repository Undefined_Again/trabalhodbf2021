package com.davysalgado.atividade.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;
    private String sobrenome;
    private String nickname;
    private int idade;
    private String cep;
    private String bairro;
    private String cidade;
    private String rua;
    private String pais;
    private String uf;
    private String genero;
    private String email;
    private String telefone;
    private String data_nascimento;
    private String escolaridade;
    private String senha;
}
